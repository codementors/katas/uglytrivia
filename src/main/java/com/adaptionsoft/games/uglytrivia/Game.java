package com.adaptionsoft.games.uglytrivia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Game {

  private static final int FIRST_INDEX = 0;
  private static final String CATEGORY_NAME_POP = "Pop";
  private static final String CATEGORY_NAME_SCIENCE = "Science";
  private static final String CATEGORY_NAME_SPORTS = "Sports";
  private static final String CATEGORY_NAME_ROCK = "Rock";

  private Map<String, List<String>> categoriesMapping = new HashMap<>();
  {
    categoriesMapping.put(CATEGORY_NAME_POP, new ArrayList<>());
    categoriesMapping.put(CATEGORY_NAME_SCIENCE, new ArrayList<>());
    categoriesMapping.put(CATEGORY_NAME_SPORTS, new ArrayList<>());
    categoriesMapping.put(CATEGORY_NAME_ROCK, new ArrayList<>());
  }

  List<String> players = new ArrayList<>();
  int[] places = new int[6];
  int[] purses = new int[6];
  boolean[] inPenaltyBox = new boolean[6];

  int currentPlayer = 0;
  boolean isGettingOutOfPenaltyBox;

  private String[] categories = new String[] {CATEGORY_NAME_POP, CATEGORY_NAME_SCIENCE, CATEGORY_NAME_SPORTS, CATEGORY_NAME_ROCK};

  public Game() {
    for (int i = 0; i < 50; i++) {
      categoriesMapping.get(CATEGORY_NAME_POP).add("Pop Question " + i);
      categoriesMapping.get(CATEGORY_NAME_SCIENCE).add("Science Question " + i);
      categoriesMapping.get(CATEGORY_NAME_SPORTS).add("Sports Question " + i);
      categoriesMapping.get(CATEGORY_NAME_ROCK).add(createRockQuestion(i));
    }
  }

  public String createRockQuestion(int index) {
    return "Rock Question " + index;
  }

  public boolean isPlayable() {
    return (howManyPlayers() >= 2);
  }

  public boolean add(String playerName) {

    players.add(playerName);
    places[howManyPlayers()] = 0;
    purses[howManyPlayers()] = 0;
    inPenaltyBox[howManyPlayers()] = false;

    System.out.println(playerName + " was added");
    System.out.println("They are player number " + players.size());
    return true;
  }

  public int howManyPlayers() {
    return players.size();
  }

  public void roll(int roll) {
    String player = players.get(currentPlayer);
    int place = places[currentPlayer];
    boolean penalty = inPenaltyBox[currentPlayer];

    System.out.println(player + " is the current player");
    System.out.println("They have rolled a " + roll);

    if (penalty) {
      if (roll % 2 != 0) {
        isGettingOutOfPenaltyBox = true;

        System.out.println(player + " is getting out of the penalty box");
        place = place + roll;
        if (place > 11) {
          place = place - 12;
        }

        System.out.println(player
                + "'s new location is "
                + place);
        System.out.println("The category is " + currentCategory());
        askQuestion();
      } else {
        System.out.println(player + " is not getting out of the penalty box");
        isGettingOutOfPenaltyBox = false;
      }

    } else {

      place = place + roll;
      if (place > 11) {
        place = place - 12;
      }

      System.out.println(player
              + "'s new location is "
              + place);
      System.out.println("The category is " + currentCategory());
      askQuestion();
    }

  }

  private void askQuestion() {
    List<String> questions = categoriesMapping.get(currentCategory());
    if (questions != null) {
      System.out.println(questions.remove(FIRST_INDEX));
    }
  }

  private String currentCategory() {
    int place = places[currentPlayer];
    int categoryIndex = place % categories.length;
    return categories[categoryIndex];
  }

  public boolean wasCorrectlyAnswered() {
    if (inPenaltyBox[currentPlayer]) {
      if (isGettingOutOfPenaltyBox) {
        System.out.println("Answer was correct!!!!");
        purses[currentPlayer]++;
        System.out.println(players.get(currentPlayer)
                + " now has "
                + purses[currentPlayer]
                + " Gold Coins.");

        boolean winner = didPlayerWin();
        currentPlayer++;
        if (currentPlayer == players.size()) {
          currentPlayer = 0;
        }

        return winner;
      } else {
        currentPlayer++;
        if (currentPlayer == players.size()) {
          currentPlayer = 0;
        }
        return true;
      }

    } else {

      System.out.println("Answer was corrent!!!!");
      purses[currentPlayer]++;
      System.out.println(players.get(currentPlayer)
              + " now has "
              + purses[currentPlayer]
              + " Gold Coins.");

      boolean winner = didPlayerWin();
      currentPlayer++;
      if (currentPlayer == players.size()) {
        currentPlayer = 0;
      }

      return winner;
    }
  }

  public boolean wrongAnswer() {
    System.out.println("Question was incorrectly answered");
    System.out.println(players.get(currentPlayer) + " was sent to the penalty box");
    inPenaltyBox[currentPlayer] = true;

    currentPlayer++;
    if (currentPlayer == players.size()) {
      currentPlayer = 0;
    }
    return true;
  }

  private boolean didPlayerWin() {
    return !(purses[currentPlayer] == 6);
  }
}
