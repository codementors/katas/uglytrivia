package com.adaptionsoft.games.trivia.runner;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class GameRunnerTest {

  public static final String LINEFEED_REGEX = "\\r\\n?";
  private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
  private final PrintStream originalOut = System.out;

  @Before
  public void setUpStreams() {
    System.setOut(new PrintStream(outContent));
  }

  @Test
  public void goldenMaster() throws Exception {
    GameRunner.executeGame(new Random(0L));

    String expectedGameOutput = Files.readString(Path.of(GameRunnerTest.class.getResource("expected-game-output").toURI()));
    assertEquals(expectedGameOutput, outContent.toString().replaceAll(LINEFEED_REGEX, "\n"));
  }

  @After
  public void restoreStreams() {
    System.setOut(originalOut);
  }
}
